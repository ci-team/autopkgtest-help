# autopkgtest help

Want some help with autopkgtest for your package? Ask here by opening an issue,
and the CI team will try to help you.

## Relevant Documenation

- [autopkgtest: how to write tests][README.package-tests]
- [autopkgtest: how to run tests][README.running-tests]
- [autodep8: implicit autopkgtest for classes of packages][autodep8]
- [ci.debian.net documentation](https://ci.debian.net/doc/)

[README.package-tests]: https://salsa.debian.org/ci-team/autopkgtest/-/blob/master/doc/README.package-tests.rst
[README.running-tests]: https://salsa.debian.org/ci-team/autopkgtest/-/blob/master/doc/README.running-tests.rst
[autodep8]: https://salsa.debian.org/ci-team/autodep8/-/blob/master/README.md

## Asking questions here

Valid requests:

- _"I want to add autopkgtest to package X. X is a tool that [...]  and it works
  by [...]. How should I approach testing it?"_

  It's OK if you have no idea where to start. But at least try to describe your
  package, what it does and how it works so we can try to help you.

- _"I started writing autopkgtest for X, here is my current work in progress
  [link]. But I encountered problem Y. How to I move forward?"_

  If you already have an autopkgtest but is having trouble making it work as
  you think it should, you can also ask here.

Invalid requests:

- _"Please write autopkgtest for my package X for me"_.

  As with anything else in free software, please show appreciation for other
  people's time, and do your own research first. If you pose your question with
  enough details (see above) and make it interesting, it _may_ be that whoever
  answers will write at least a basic structure for you, but as the maintainer
  you are still the expert in the package and what tests are relevant.
